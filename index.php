<?php

/**
 * Plugin Name:     kk Star Ratings
 * Plugin Slug:     kk-star-ratings
 * Plugin Nick:     kksr
 * Plugin URI:      https://github.com/kamalkhan/kk-star-ratings
 * Description:     Allow blog visitors to involve and interact more effectively with your website by rating posts.
 * Author:          Kamal Khan
 * Author URI:      http://bhittani.com
 * Text Domain:     kk-star-ratings
 * Domain Path:     /languages
 * Version:         5.2.0
 * License:         GPLv2 or later
 */

use function Bhittani\StarRating\core\functions\filter;
use function Bhittani\StarRating\functions\cast;

if ( ! defined( 'ABSPATH' ) ) {
	http_response_code( 404 );
	exit();
}

define( 'KK_STAR_RATINGS', __FILE__ );


if ( function_exists( 'kksr_freemius' ) ) {
	kksr_freemius()->set_basename( true, __FILE__ );
} else {
	if ( ! function_exists( 'kksr_freemius' ) ) {
		require_once __DIR__ . '/freemius.php';
	}

	require_once __DIR__ . '/src/index.php';
	require_once __DIR__ . '/src/core/index.php';

}

function is_amp() {
	return function_exists( 'is_amp_endpoint' ) && is_amp_endpoint();
}

function get_amp_star( ) {
	if ( function_exists( 'is_amp_endpoint' ) && is_amp_endpoint() ) {
		global $post;
		$_kksr_avg_default   = get_post_meta( $post->ID, '_kksr_avg_default', true ) ?? 0;
		$_kksr_count_default = get_post_meta( $post->ID, '_kksr_count_default', true ) ?? 0;

		$avg_count = (int) floor( $_kksr_avg_default );
		$avg       = round( $_kksr_avg_default, '2' );
		?>

		<form
				id="rating"
				class="<?php echo $avg_count === 0 ? 'zero' : '' ?>"
				method="post"
				action-xhr="\wp-admin\admin-ajax.php"
				target="_blank">

			<fieldset class="rating">
				<input
						name="rating" type="radio"
						id="rating1"
						value="1"
					<?php checked( $avg_count, 1 ); ?>>
				<label for="rating1" title="1 stars" on="tap:rating.submit"></label>
				<input
						name="rating"
						type="radio"
						id="rating2"
						value="2"
					<?php checked( $avg_count, 3 ); ?>>
				<label for="rating2" title="2 stars" on="tap:rating.submit"></label>
				<input
						name="rating"
						type="radio"
						id="rating3"
						value="3"
					<?php checked( $avg_count, 3 ); ?>>
				<label for="rating3" title="3 stars"></label>
				<input
						name="rating"
						type="radio"
						id="rating4"
						value="4"
					<?php checked( $avg_count, 4 ); ?>>
				<label id="rating4" for="rating4" title="4 stars" on="tap:rating.submit"></label>
				<input
						name="rating"
						type="radio"
						id="rating5"
						value="5"
					<?php checked( $avg_count, 5 ); ?>>
				<label for="rating5" title="5 stars" on="tap:rating.submit"></label>
			</fieldset>
			<input type="hidden" name="action" value="kk-star-ratings">
			<input type="hidden" name="payload[id]" value="<?php echo get_the_ID() ?>">
			<input type="hidden" name="payload[slug]" value="default">
			<input type="hidden" name="nonce"
				   value="<?php echo wp_create_nonce( 'Bhittani\StarRating\core\wp\actions\wp_ajax_kk_star_ratings' ) ?>">
			<div class="kksr-legend-amp">
				<?php
				echo $avg . '/5 - (' . $_kksr_count_default . ' Bewertungen)';
				?>
			</div>
			<div submit-success class="submit-success">
				<template type="amp-mustache">
					<p class="success">Danke für die Sternebewertung!</p>
				</template>
			</div>
			<div submit-error class="submit-error">
				<template type="amp-mustache">
					<p class="error">{{error}}</p>
				</template>
			</div>
		</form>

		<?php
	}
}

/**
 * Checks if the user voted for this post.
 *
 * @param int|string $post_id post id.
 *
 * @return bool
 */
function unique_user( $post_id ) {
	$user_id    = filter_var( wp_unslash( $_SERVER['REMOTE_ADDR'] ), FILTER_VALIDATE_IP );
	$users_vote = $_SESSION['vote'];
	if ( ! empty( $users_vote ) ) {
		foreach ( $users_vote as $post_vote ) {
			foreach ($post_vote as $ip => $vote){
				if ( (int) $post_id === $vote && $ip === $user_id ) {
					return false;
				}
			}
		}
	}

	return true;
}

